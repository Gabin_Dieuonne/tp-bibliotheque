package com.example.test;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class InterfaceConnectionController {
    @FXML
    public TextField idTextfield = new TextField();
    @FXML
    public PasswordField passwordField = new PasswordField();
    @FXML
    public Label testLabel = new Label();
    public User user;
    @FXML
    public void onConnectionButtonClick() {try{
        Stage connectionStage = (Stage) idTextfield.getScene().getWindow();
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection con= DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/DbBiblio","root","Sobavo+55927");
        int id = Integer.parseInt(idTextfield.getText());
        PreparedStatement stmt = con.prepareStatement("SELECT * FROM Users WHERE Id = ?");
        stmt.setInt(1, id);
        ResultSet rs = stmt.executeQuery();
        if(rs.next()){
            String nom = rs.getString(2);
            String prenom = rs.getString(3);
            String mail = rs.getString(4);
            int categorieId = rs.getInt(5);
            String password = rs.getString(6);
            user = new User(id, password, nom, prenom, mail, categorieId);
            if (user.connection(passwordField.getText())){
                testLabel.setText("Connection réussie");
                FXMLLoader fxmlLoader;
                InterfaceUtilisateurController guiUser;
                if (categorieId == 1) {
                    fxmlLoader = new FXMLLoader(getClass().getResource("interface-admin.fxml"));
                    Parent root1 = (Parent) fxmlLoader.load();
                    guiUser = fxmlLoader.getController();
                    guiUser.setUtilisateur(user);
                    guiUser.onMenuDeroulantClick();
                    Stage stage = new Stage();
                    stage.setTitle("Livres à emprunter");
                    stage.setScene(new Scene(root1));
                    stage.show();
                    connectionStage.close();
                }
                else {
                    fxmlLoader = new FXMLLoader(getClass().getResource("interface-utilisateur.fxml"));
                    Parent root1 = (Parent) fxmlLoader.load();
                    guiUser = fxmlLoader.getController();
                    guiUser.setUtilisateur(user);
                    guiUser.onMenuDeroulantClick();
                    Stage stage = new Stage();
                    stage.setTitle("Livres disponibles");
                    stage.setScene(new Scene(root1));
                    stage.show();
                    connectionStage.close();
                }}
            else {
                testLabel.setText("Mot de passe ou identifiant incorrect");
                passwordField.setText("");
            }}
        con.close();
    }catch(Exception e){ System.out.println(e);}
    }

}