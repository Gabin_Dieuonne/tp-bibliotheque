package com.example.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Objects;
import java.util.Vector;

public class Livre {
    public int id = -1;
    public String titre;
    public String dateParution;
    public String motCle1 = "";
    public String motCle2 = "";
    public String motCle3 = "";
    public String motCle4 = "";
    public String motCle5 = "";

    public Livre(int id) {
        this.id = id;
    }

    public void livreFromId(int id){
        this.id = id;
        try{
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/DbBiblio", "root", "Sobavo+55927");
            PreparedStatement stmt = con.prepareStatement("SELECT * FROM Livres WHERE Livres.Id = ?");
            stmt.setInt(1,id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                this.titre = rs.getString(2);
                this.dateParution = rs.getString(3);
                this.motCle1 = rs.getString(4);
                this.motCle2 = rs.getString(5);
                this.motCle3 = rs.getString(6);
                this.motCle4 = rs.getString(7);
                this.motCle5 = rs.getString(8);
            }
        }
        catch(Exception e){
            System.out.println(e);
        }
    }

    public Livre(int id, String titre, String dateParution,String motCle1, String motCle2, String motCle3, String motCle4, String motCle5) {
        this.dateParution = dateParution;
        this.id = id;
        this.motCle1 = motCle1;
        this.motCle2 = motCle2;
        this.motCle3 = motCle3;
        this.motCle4 = motCle4;
        this.motCle5 = motCle5;
        this.titre = titre;
    }
    public String getStringMotsCles(){
        String str = "";
        if(!Objects.equals(motCle1, "")){
            str+= motCle1;
        }
        if(!Objects.equals(motCle2, "")){
            str += " ," + motCle2;
        }
        if(!Objects.equals(motCle3, "")){
            str += " ," + motCle3;
        }
        if(!Objects.equals(motCle4, "")){
            str+=" ,"+motCle4;
        }
        if(!Objects.equals(motCle5, "")){
            str+=" ,"+motCle5;
        }
        return str;
    }
    public String getAuteurs() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/DbBiblio", "root", "Sobavo+55927");
            PreparedStatement stmt = con.prepareStatement("SELECT Nom, Prenom FROM Auteurs JOIN EstAuteurDe ON Auteurs.Id = EstAuteurDe.IdAuteur JOIN Livres ON Livres.Id = EstAuteurDe.IdLivre WHERE Livres.Id = ?");
            stmt.setInt(1, this.id);
            ResultSet rs = stmt.executeQuery();
            StringBuilder strAuteurs = new StringBuilder();
            while (rs.next()){
                String nomAuteur = rs.getString(1);
                String prenomAuteur = rs.getString(2);
                String auteur = nomAuteur + " " + prenomAuteur;
                strAuteurs.append(auteur);
            }
            return strAuteurs.toString();
        } catch
        (Exception e) {
            System.out.println(e);
            return null;
        }
    }
    public Edition getEdition() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/DbBiblio", "root", "Sobavo+55927");
            PreparedStatement stmt = con.prepareStatement("SELECT * FROM Edition JOIN EstEdite ON Edition.ISBN = EstEdite.ISBN JOIN Livres ON Livres.Id = EstEdite.IdLivre WHERE Livres.Id = ?");
            stmt.setInt(1, this.id);
            ResultSet rs = stmt.executeQuery();
            int isbn = -1;
            String nomEditeur = new String();
            String anneeEdition = new String();
            while (rs.next()){
                isbn = rs.getInt(1);
                nomEditeur = rs.getString(2);
                anneeEdition = rs.getString(3);
            }
            Edition edition = new Edition(isbn, nomEditeur, anneeEdition);
            return edition;
        } catch
        (Exception e) {
            System.out.println(e);
            return null;
        }
    }
    public Boolean estEmprunte(){
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/DbBiblio", "root", "Sobavo+55927");
            PreparedStatement stmt = con.prepareStatement("SELECT COUNT(IdEmprunt) FROM EmpruntsEnCours WHERE IdLivre = ?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            int emprunts = 0;
            if(rs.next()) {
                emprunts = rs.getInt(1);
            }
            return (emprunts != 0);
        } catch
        (Exception e) {
            System.out.println(e);
            return null;
        }
    }
    private int getMaxId(){
        try{
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/DbBiblio", "root", "Sobavo+55927");
            PreparedStatement stmt = con.prepareStatement("SELECT MAX(Id) FROM Livres");
            ResultSet rs = stmt.executeQuery();
            int maxId = 1;
            if (rs.next()){
                maxId = rs.getInt(1);
            }
            return maxId;
        }
        catch(Exception e){
            System.out.println(e);
            return 1;
        }
    }
    public Livre(){
        this.id = getMaxId()+1;
    }
}
