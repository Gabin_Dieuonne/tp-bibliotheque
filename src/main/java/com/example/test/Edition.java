package com.example.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

public class Edition {
    int isbn = -1;
    String nomEditeur;
    String dateEdition;
    public Edition(int isbn, String nomEditeur, String dateEdition){
        this.isbn = isbn;
        this.dateEdition = dateEdition;
        this.nomEditeur = nomEditeur;
    }

    public Boolean alreadyExists(){
        try{
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/DbBiblio", "root", "Sobavo+55927");
            PreparedStatement stmt = con.prepareStatement("SELECT COUNT(ISBN) FROM Edition WHERE ISBN = ?");
            stmt.setInt(1, isbn);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                return rs.getInt(1)>0;
            }
            return false;
        }
        catch(Exception e){
            System.out.println(e);
            return false;
        }
    }

    public String nomEditeur() {
        return(this.nomEditeur);
    }
}
