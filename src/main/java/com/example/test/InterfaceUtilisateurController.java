package com.example.test;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;

public class InterfaceUtilisateurController {
    public void setUtilisateur(User utilisateur) {
        this.utilisateur = utilisateur;
    }

    User utilisateur = new User();
    public InterfaceUtilisateurController(){
    }
    @FXML
    Button rechercher = new Button();
    @FXML
    Button accesEmprunts = new Button();
    @FXML
    TableView<LivreTableau> tableLivres = new TableView<LivreTableau>();
    @FXML
    TableColumn<LivreTableau,String> colonneTitre = new TableColumn<LivreTableau,String>();
    @FXML
    TableColumn<LivreTableau, String> colonneAuteur = new TableColumn<LivreTableau,String>();
    @FXML
    TableColumn<LivreTableau,String> colonneDate = new TableColumn<LivreTableau,String>();
    @FXML
    TableColumn<LivreTableau,String> colonneEdition = new TableColumn<LivreTableau,String>();
    @FXML
    TableColumn<LivreTableau, String> colonneMotsCles = new TableColumn<LivreTableau, String>();
    @FXML
    TableColumn<LivreTableau, String> colonneStatut = new TableColumn<LivreTableau, String>();
    @FXML
    SplitMenuButton menuDeroulantRecherche = new SplitMenuButton();
    @FXML
    TextField searchField = new TextField();
    @FXML
    Label empruntsDisponibles = new Label();
    @FXML
    TextField textFieldTitre = new TextField();
    @FXML
    TextField textFieldPremiereParution = new TextField();
    @FXML
    TextField textFieldMotCle1 = new TextField();
    @FXML
    TextField textFieldMotCle2 = new TextField();
    @FXML
    TextField textFieldMotCle3 = new TextField();
    @FXML
    TextField textFieldMotCle4 = new TextField();
    @FXML
    TextField textFieldMotCle5 = new TextField();
    @FXML
    TextField textFieldISBN = new TextField();
    @FXML
    TextField textFieldNomEditeur = new TextField();
    @FXML
    TextField textFieldAnneeEdition = new TextField();
    @FXML
    TextField textFieldNomAuteur = new TextField();
    @FXML
    TextField textFieldPrenomAuteur = new TextField();
    @FXML
    TextField textFieldAnneeNaissance = new TextField();

    @FXML
    public void onMenuItemClick(ActionEvent event) {
        MenuItem item = (MenuItem) event.getSource();
        String str = item.getText();
        menuDeroulantRecherche.setText(str);
    }

    @FXML
    public void onMenuDeroulantClick(){
        try {
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/DbBiblio", "root", "Sobavo+55927");
            String searched = searchField.getText();
            String search = "%" + searched + "%";
        colonneTitre.setCellValueFactory(new PropertyValueFactory<LivreTableau, String>("titre"));
        colonneAuteur.setCellValueFactory(new PropertyValueFactory<LivreTableau, String>("auteur"));
        colonneDate.setCellValueFactory(new PropertyValueFactory<LivreTableau, String>("dateDeParution"));
        colonneEdition.setCellValueFactory(new PropertyValueFactory<LivreTableau, String>("edition"));
        colonneMotsCles.setCellValueFactory(new PropertyValueFactory<LivreTableau, String>("motsCles"));
        colonneStatut.setCellValueFactory(new PropertyValueFactory<LivreTableau, String>("statut"));
        ObservableList<LivreTableau> livresARentrer = FXCollections.observableArrayList();
        String searchType = menuDeroulantRecherche.getText();
        PreparedStatement stmt = null;
            switch (searchType) {
                case "Titre" -> {
                    stmt = con.prepareStatement("SELECT * FROM Livres WHERE Titre LIKE ? ORDER BY Titre");
                    stmt.setString(1, search);
                }
                case "Auteur" -> {
                    stmt = con.prepareStatement("SELECT Livres.Id, Livres.Titre, Livres.AnneePremiereParution, Livres.MotCle1, Livres.MotCle2, Livres.MotCle3, Livres.MotCLe4, Livres.MotCle5 FROM Livres JOIN EstAuteurDe ON Livres.Id = EstAuteurDe.IdLivre JOIN Auteurs ON EstAuteurDe.IdAuteur = Auteurs.Id WHERE Prenom LIKE ? UNION SELECT Livres.Id, Livres.Titre, Livres.AnneePremiereParution, Livres.MotCle1, Livres.MotCle2, Livres.MotCle3, Livres.MotCLe4, Livres.MotCle5 FROM Livres JOIN EstAuteurDe ON Livres.Id = EstAuteurDe.IdLivre JOIN Auteurs ON EstAuteurDe.IdAuteur = Auteurs.Id WHERE Nom LIKE ? ORDER BY Titre ");
                    stmt.setString(1, search);
                    stmt.setString(2, search);
                }
                case "Edition" -> {
                    stmt = con.prepareStatement("SELECT Livres.Id, Livres.Titre, Livres.AnneePremiereParution, Livres.MotCle1, Livres.MotCle2, Livres.MotCle3, Livres.MotCLe4, Livres.MotCle5 FROM Livres JOIN EstEdite ON Livres.Id = EstEdite.IdLivre JOIN Edition ON EstEdite.ISBN = Edition.ISBN WHERE NomEditeur LIKE ? ORDER BY Titre ");
                    stmt.setString(1, search);
                }
                default -> {
                    stmt = con.prepareStatement("SELECT Livres.Id, Livres.Titre, Livres.AnneePremiereParution, Livres.MotCle1, Livres.MotCle2, Livres.MotCle3, Livres.MotCLe4, Livres.MotCle5 FROM Livres WHERE Titre LIKE ? UNION SELECT Livres.Id, Livres.Titre, Livres.AnneePremiereParution, Livres.MotCle1, Livres.MotCle2, Livres.MotCle3, Livres.MotCLe4, Livres.MotCle5 FROM Livres JOIN EstAuteurDe ON Livres.Id = EstAuteurDe.IdLivre JOIN Auteurs ON EstAuteurDe.IdAuteur = Auteurs.Id WHERE Prenom LIKE ? UNION SELECT Livres.Id, Livres.Titre, Livres.AnneePremiereParution, Livres.MotCle1, Livres.MotCle2, Livres.MotCle3, Livres.MotCLe4, Livres.MotCle5 FROM Livres JOIN EstAuteurDe ON Livres.Id = EstAuteurDe.IdLivre JOIN Auteurs ON EstAuteurDe.IdAuteur = Auteurs.Id WHERE Nom LIKE ? UNION SELECT Livres.Id, Livres.Titre, Livres.AnneePremiereParution, Livres.MotCle1, Livres.MotCle2, Livres.MotCle3, Livres.MotCLe4, Livres.MotCle5 FROM Livres JOIN EstEdite ON Livres.Id = EstEdite.IdLivre JOIN Edition ON EstEdite.ISBN = Edition.ISBN WHERE NomEditeur LIKE ? ORDER BY Titre");
                    stmt.setString(1, search);
                    stmt.setString(2, search);
                    stmt.setString(3, search);
                    stmt.setString(4, search);
                }
            }
            ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            int idLivre = rs.getInt(1);
            String titre = rs.getString(2);
            String dateParution = rs.getString(3);
            String motCle1 = rs.getString(4);
            String motCle2 = rs.getString(5);
            String motCle3 = rs.getString(6);
            String motCle4 = rs.getString(7);
            String motCle5 = rs.getString(8);
            Livre livre = new Livre(idLivre, titre, dateParution, motCle1, motCle2, motCle3, motCle4, motCle5);
            String auteur = livre.getAuteurs();
            Edition edition = livre.getEdition();
            String motsCles = livre.getStringMotsCles();
            if(livre.estEmprunte()){
                livresARentrer.add(new LivreTableau(titre, auteur, dateParution, edition.nomEditeur(), motsCles, "Indisponible", idLivre));}
            else{
                livresARentrer.add(new LivreTableau(titre, auteur, dateParution, edition.nomEditeur(), motsCles, "Disponible", idLivre));}
        }
        tableLivres.setItems(livresARentrer);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @FXML
    public void onEmprunterButtonClicked(){
        LivreTableau livreAEmprunter  = tableLivres.getSelectionModel().getSelectedItem();
        if (livreAEmprunter == null){
            empruntsDisponibles.setText("Vous n'avez pas séléctionné de livre");
        } else {
            Livre livre = new Livre(livreAEmprunter.getIdLivre());
            if (livre.estEmprunte()) {
                empruntsDisponibles.setText(("Ce livre est déja emprunté"));
            } else{
                if(utilisateur.getNombreEmpruntsDispos()>0){
                    try{
                        Connection con = DriverManager.getConnection(
                                "jdbc:mysql://localhost:3306/DbBiblio", "root", "Sobavo+55927");
                        Date today = Date.valueOf(LocalDate.now());
                        PreparedStatement stmt = con.prepareStatement("INSERT INTO EmpruntsEnCours VALUES (?,?,?,?,?)");
                        Emprunts emprunt = new Emprunts(livreAEmprunter.idLivre, utilisateur.id);
                        stmt.setInt(1, emprunt.idEmprunt);
                        stmt.setInt(2, emprunt.idUser);
                        stmt.setInt(3, emprunt.idLivre);
                        stmt.setDate(4, today);
                        Calendar cal = Calendar.getInstance();
                        try{
                            cal.setTime(today);
                        }
                        catch(Exception e){
                            System.out.println(e);
                        }
                        cal.add(Calendar.DAY_OF_MONTH, utilisateur.getDureeEmprunts());
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        String dateFin = sdf.format(cal.getTime());
                        stmt.setString(5, dateFin);
                        stmt.addBatch();
                        stmt.executeBatch();
                        empruntsDisponibles.setText("Emprunt effectué");
                        onMenuDeroulantClick();
                    }
                    catch (Exception e) {
                        System.out.println(e);
                    }
                }
                else{
                    empruntsDisponibles.setText("Vous n'avez plus d'emprunts disponibles");
                }
            }
        }
    }
    public void onAccesEmpruntsClick() throws IOException {
        Stage connectionStage = (Stage) tableLivres.getScene().getWindow();
        FXMLLoader fxmlLoader = null;
        if(utilisateur.categorieId == 1) {
            fxmlLoader = new FXMLLoader(getClass().getResource("interface-admin-emprunts.fxml"));
        }
        else{
            fxmlLoader = new FXMLLoader(getClass().getResource("interface-utilisateur-emprunts.fxml"));
        }
        Parent root1 = (Parent) fxmlLoader.load();
        InterfaceUtilisateurEmpruntsController guiUserEmprunts = fxmlLoader.getController();
        guiUserEmprunts.setUtilisateur(utilisateur);
        guiUserEmprunts.onMenuDeroulantEmpruntsClick();
        Stage stage = new Stage();
        stage.setTitle("Emprunts");
        stage.setScene(new Scene(root1));
        stage.show();
        connectionStage.close();
    }

    public void onAccesUsersClick() throws IOException, SQLException {
        Stage connectionStage = (Stage) tableLivres.getScene().getWindow();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("gestion-users.fxml"));
        Parent root1 = (Parent) fxmlLoader.load();
        InterfaceAdminGestionUsersController guiAdminUsers = fxmlLoader.getController();
        guiAdminUsers.setUtilisateur(utilisateur);
        Connection con = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/DbBiblio", "root", "Sobavo+55927");
        guiAdminUsers.onRechercherButtonClick();
        PreparedStatement stmtCat = con.prepareStatement("SELECT Nom FROM Categorie");
        ResultSet rsCat = stmtCat.executeQuery();
        ObservableList<String> listeCat = FXCollections.observableArrayList();
        while(rsCat.next()){
            String nom = rsCat.getString(1);
            listeCat.add(nom);
        }
        guiAdminUsers.listeCategories.setItems(listeCat);
        guiAdminUsers.listeCategories2.setItems(listeCat);
        Stage stage = new Stage();
        stage.setTitle("Gestion des utilisateurs");
        stage.setScene(new Scene(root1));
        stage.show();
        connectionStage.close();
    }
    @FXML
    public void onSupprimerButtonClicked(){
        LivreTableau livreASupprimer  = tableLivres.getSelectionModel().getSelectedItem();
        Livre livre = new Livre(livreASupprimer.getIdLivre());
        if (livreASupprimer == null){
            empruntsDisponibles.setText("Vous n'avez pas séléctionné de livre");
        }
        else {
            if(livre.estEmprunte()){
                empruntsDisponibles.setText("Ce livre est emprunté, vous ne pouvez pas le supprimer");
            }
            else {
                try {
                    Connection con = DriverManager.getConnection(
                            "jdbc:mysql://localhost:3306/DbBiblio", "root", "Sobavo+55927");
                    PreparedStatement stmtLivres = con.prepareStatement("DELETE FROM Livres WHERE Id = ?");
                    PreparedStatement stmtAssociation = con.prepareStatement("DELETE FROM EstEdite WHERE IdLivre = ?");
                    PreparedStatement stmtEmprunts = con.prepareStatement("DELETE FROM HistoriqueEmprunts WHERE IdLivre = ?");
                    PreparedStatement stmtAuteur = con.prepareStatement("DELETE FROM EstAuteurDe WHERE IdLivre = ?");
                    stmtLivres.setInt(1, livreASupprimer.getIdLivre());
                    stmtAssociation.setInt(1, livreASupprimer.getIdLivre());
                    stmtEmprunts.setInt(1, livreASupprimer.getIdLivre());
                    stmtAuteur.setInt(1, livreASupprimer.getIdLivre());
                    stmtLivres.execute();
                    stmtAssociation.execute();
                    stmtEmprunts.execute();
                    stmtAuteur.execute();
                    empruntsDisponibles.setText("Suppression effectuée");
                    onMenuDeroulantClick();
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
    }
    @FXML
    public void onCreerLivreClick(){
        String titre = textFieldTitre.getText();
        String anneePremiereParution = textFieldPremiereParution.getText();
        String motCle1 = textFieldMotCle1.getText();
        String motCle2 = textFieldMotCle2.getText();
        String motCle3 = textFieldMotCle3.getText();
        String motCle4 = textFieldMotCle4.getText();
        String motCle5 = textFieldMotCle5.getText();
        int isbn = Integer.parseInt(textFieldISBN.getText());
        String nomEditeur = textFieldNomEditeur.getText();
        String anneeEdition = textFieldAnneeEdition.getText();
        String nomAuteur = textFieldNomAuteur.getText();
        String prenomAuteur = textFieldPrenomAuteur.getText();
        String anneeNaissance = textFieldAnneeEdition.getText();
        try{
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/DbBiblio", "root", "Sobavo+55927");
            Livre livre = new Livre();
            PreparedStatement stmtLivre = con.prepareStatement("INSERT INTO Livres VALUES(?,?,?,?,?,?,?,?)");
            stmtLivre.setInt(1, livre.id);
            stmtLivre.setString(2, titre);
            stmtLivre.setString(3, anneePremiereParution);
            stmtLivre.setString(4, motCle1);
            stmtLivre.setString(5, motCle2);
            stmtLivre.setString(6, motCle3);
            stmtLivre.setString(7, motCle4);
            stmtLivre.setString(8, motCle5);
            stmtLivre.addBatch();
            stmtLivre.executeBatch();
            Auteur auteur = new Auteur(nomAuteur, prenomAuteur, anneeNaissance);
            if(!auteur.auteurExist()){
                PreparedStatement stmtAuteur = con.prepareStatement("INSERT INTO Auteurs VALUES(?,?,?,?)");
                stmtAuteur.setInt(1, auteur.id);
                stmtAuteur.setString(2, nomAuteur);
                stmtAuteur.setString(3, prenomAuteur);
                stmtAuteur.setString(4, anneeNaissance);
                stmtAuteur.addBatch();
                stmtAuteur.executeBatch();
                PreparedStatement stmtLivreAuteur = con.prepareStatement("INSERT INTO EstAuteurDe VALUES (?,?)");
                stmtLivreAuteur.setInt(1, livre.id);
                stmtLivreAuteur.setInt(2, auteur.id);
                stmtLivreAuteur.addBatch();
                stmtLivreAuteur.executeBatch();
            }
            else{
                auteur.getAuteurFromAllButId(nomAuteur, prenomAuteur, anneeNaissance);
                PreparedStatement stmtLivreAuteur = con.prepareStatement("INSERT INTO EstAuteurDe VALUES (?,?)");
                stmtLivreAuteur.setInt(1, livre.id);
                stmtLivreAuteur.setInt(2, auteur.id);
                stmtLivreAuteur.addBatch();
                stmtLivreAuteur.executeBatch();
            }
            Edition edition = new Edition(isbn, nomEditeur, anneeEdition);
            if(edition.alreadyExists()){
                PreparedStatement stmtEditionLivre = con.prepareStatement("INSERT INTO EstEdite VALUES (?,?)");
                stmtEditionLivre.setInt(1, livre.id);
                stmtEditionLivre.setInt(2, isbn);
                stmtEditionLivre.addBatch();
                stmtEditionLivre.executeBatch();
            }
            else{
                PreparedStatement stmtEdition = con.prepareStatement("INSERT INTO Edition VALUES (?,?,?)");
                stmtEdition.setInt(1, isbn);
                stmtEdition.setString(2, nomEditeur);
                stmtEdition.setString(3, anneeEdition);
                stmtEdition.addBatch();
                stmtEdition.executeBatch();
                PreparedStatement stmtEditionLivre = con.prepareStatement("INSERT INTO EstEdite VALUES (?,?)");
                stmtEditionLivre.setInt(1, livre.id);
                stmtEditionLivre.setInt(2, isbn);
                stmtEditionLivre.addBatch();
                stmtEditionLivre.executeBatch();
            }
            empruntsDisponibles.setText("Livre ajouté");
        }
        catch(Exception e){
            System.out.println(e);
        }
    }
}
