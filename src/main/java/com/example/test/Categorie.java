package com.example.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.Period;

public class Categorie {
    int id;
    String nom = "";
    int nbEmpruntsMax;
    int dureeEmpruntsMax;
    public Categorie(int id, String nom, int nbEmpruntsMax, int dureeEmpruntsMax) {
        this.id = id;
        this.nom = nom;
        this.nbEmpruntsMax = nbEmpruntsMax;
        this.dureeEmpruntsMax = dureeEmpruntsMax;
    }

    public Categorie() {
    }
    public void getCategorieFromString(String nom){
        this.nom = nom;
        try{
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/DbBiblio", "root", "Sobavo+55927");
            PreparedStatement stmt = con.prepareStatement("SELECT Id, MaxEmprunts, MaxDuree FROM Categorie WHERE Nom = ?");
            stmt.setString(1, nom);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                this.id = rs.getInt(1);
                this.nbEmpruntsMax = rs.getInt(2);
                this.dureeEmpruntsMax = rs.getInt(3);
            }
        }
        catch(Exception e){
            System.out.println(e);
        }
    }

    public int getId() {
        return id;
    }
}
