package com.example.test;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.*;
import java.util.Objects;

public class InterfaceAdminGestionUsersController {
    User utilisateur = new User();
    public void setUtilisateur(User utilisateur) {
        this.utilisateur = utilisateur;
    }
    public InterfaceAdminGestionUsersController(){}
    @FXML
    TableView<User> tableUsers = new TableView<User>();
    @FXML
    TableColumn<User,Integer> colonneId = new TableColumn<User, Integer>();
    @FXML
    TableColumn<User,String> colonneNom = new TableColumn<User, String>();
    @FXML
    TableColumn<User,String> colonnePrenom = new TableColumn<User, String>();
    @FXML
    TableColumn<User,String> colonneEmail = new TableColumn<User, String>();
    @FXML
    TableColumn<User, Integer> colonneCategorie = new TableColumn<User, Integer>();
    @FXML
    ChoiceBox<String> listeCategories;
    @FXML
    ChoiceBox<String> listeCategories2;
    @FXML
    Label comLabel = new Label();
    @FXML
    TextField searchField = new TextField();
    @FXML
    TextField textFieldNom = new TextField();
    @FXML
    TextField textFieldPrenom = new TextField();
    @FXML
    TextField textFieldEmail = new TextField();
    @FXML
    TextField textFieldPassword = new TextField();
    @FXML
    public void onChangerCategorieClick(){
        User user = tableUsers.getSelectionModel().getSelectedItem();
        if (user == null){
            comLabel.setText("Vous n'avez pas séléctionné d'utilisateur");
        } else {
            String nom = listeCategories.getValue();
            if (nom == null) {
                comLabel.setText(("Vous n'avez pas séléctionné de catégorie"));
            } else{
                Categorie cat = new Categorie();
                cat.getCategorieFromString(nom);
                user.setCategorieId(cat.id);
                comLabel.setText("Changement de catégorie effectué");
                onRechercherButtonClick();
            }
        }
    }
    @FXML
    public void onRechercherButtonClick(){
        try{
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/DbBiblio", "root", "Sobavo+55927");
            PreparedStatement stmt = con.prepareStatement("SELECT Id, Nom, Prenom, Email, CategorieId FROM Users WHERE Nom LIKE ? OR Prenom LIKE ? OR Email LIKE ?");
            ObservableList<User> users = FXCollections.observableArrayList();
            String search = searchField.getText();
            search = "%" + search + "%";
            stmt.setString(1, search);
            stmt.setString(2, search);
            stmt.setString(3, search);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                int id = rs.getInt(1);
                String nom = rs.getString(2);
                String prenom = rs.getString(3);
                String email = rs.getString(4);
                int categorie = rs.getInt(5);
                User user = new User(id, nom, prenom, email, categorie);
                users.add(user);
            }
            colonneId.setCellValueFactory(new PropertyValueFactory<User, Integer>("id"));
            colonneNom.setCellValueFactory(new PropertyValueFactory<User, String>("nom"));
            colonnePrenom.setCellValueFactory(new PropertyValueFactory<User, String>("prenom"));
            colonneEmail.setCellValueFactory(new PropertyValueFactory<User, String>("mail"));
            colonneCategorie.setCellValueFactory(new PropertyValueFactory<User, Integer>("categorieId"));
            tableUsers.setItems(users);
        }
        catch(Exception e){
            System.out.println(e);
        }
    }
    @FXML
    public void onCreerUtilisateurClick(){
        String nom = textFieldNom.getText();
        String prenom = textFieldPrenom.getText();
        String email = textFieldEmail.getText();
        String password = textFieldPassword.getText();
        String categorieNom = listeCategories2.getSelectionModel().getSelectedItem();
        if (Objects.equals(nom, "") || Objects.equals(prenom, "") || Objects.equals(email, "") || Objects.equals(password, "") || categorieNom == null){
            comLabel.setText("Une valeur n'a pas été selectionnée");
        }
        else{
            Categorie cat = new Categorie();
            cat.getCategorieFromString(categorieNom);
            try{
                Connection con = DriverManager.getConnection(
                        "jdbc:mysql://localhost:3306/DbBiblio", "root", "Sobavo+55927");
                PreparedStatement stmt = con.prepareStatement("INSERT INTO Users VALUES (?,?,?,?,?,?)");
                User user = new User();
                stmt.setInt(1, user.id);
                stmt.setString(6, password);
                stmt.setString(2, nom);
                stmt.setString(3, prenom);
                stmt.setString(4, email);
                stmt.setInt(5, cat.getId());
                stmt.addBatch();
                stmt.executeBatch();
                comLabel.setText("Utilisateur ajouté");
                textFieldNom.setText("");
                textFieldPrenom.setText("");
                textFieldEmail.setText("");
                textFieldPassword.setText("");
                onRechercherButtonClick();
            }
            catch(Exception e){
                System.out.println(e);
            }
        }
    }
    public void onAccesAEmprunterClick() throws IOException {
        Stage connectionStage = (Stage) tableUsers.getScene().getWindow();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("interface-admin.fxml"));
        Parent root1 = (Parent) fxmlLoader.load();
        InterfaceUtilisateurController guiUser = fxmlLoader.getController();
        guiUser.setUtilisateur(utilisateur);
        guiUser.onMenuDeroulantClick();
        Stage stage = new Stage();
        stage.setTitle("Livres à emprunter");
        stage.setScene(new Scene(root1));
        stage.show();
        connectionStage.close();
    }
    public void onAccesEmpruntsAdminClick() throws IOException {
        Stage connectionStage = (Stage) tableUsers.getScene().getWindow();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("interface-admin-emprunts.fxml"));
        Parent root1 = (Parent) fxmlLoader.load();
        InterfaceUtilisateurEmpruntsController guiUserEmprunts = fxmlLoader.getController();
        guiUserEmprunts.setUtilisateur(utilisateur);
        guiUserEmprunts.onMenuDeroulantEmpruntsClick();
        Stage stage = new Stage();
        stage.setTitle("Emprunts");
        stage.setScene(new Scene(root1));
        stage.show();
        connectionStage.close();
    }
}
