package com.example.test;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.*;

public class InterfaceUtilisateurEmpruntsController {
    User utilisateur = new User();
    public void setUtilisateur(User utilisateur) {
        this.utilisateur = utilisateur;
    }
    public InterfaceUtilisateurEmpruntsController(){}
    @FXML
    TableView<Emprunts> tableEmprunts = new TableView<Emprunts>();
    @FXML
    TableColumn<Emprunts, String> colonneTitreEmprunt = new TableColumn<Emprunts, String>();
    @FXML
    TableColumn<Emprunts, String> colonneAuteurEmprunt = new TableColumn<Emprunts, String>();
    @FXML
    TableColumn<Emprunts, Date> colonneDateDebutEmprunt = new TableColumn<Emprunts, Date>();
    @FXML
    TableColumn<Emprunts, Date> colonneDateFinEmprunt = new TableColumn<Emprunts, Date>();
    @FXML
    Label labelComm = new Label();
    @FXML
    SplitMenuButton boutonMenuDeroulant = new SplitMenuButton();
    public void onMenuItemClick(ActionEvent event) {
        MenuItem item = (MenuItem) event.getSource();
        String str = item.getText();
        boutonMenuDeroulant.setText(str);
    }

    public void onAccesAEmprunterClick() throws IOException {
        Stage connectionStage = (Stage) tableEmprunts.getScene().getWindow();
        FXMLLoader fxmlLoader = null;
        if(utilisateur.categorieId == 1){
            fxmlLoader = new FXMLLoader(getClass().getResource("interface-admin.fxml"));
        }
        else{
            fxmlLoader = new FXMLLoader(getClass().getResource("interface-utilisateur.fxml"));
        }
        Parent root1 = (Parent) fxmlLoader.load();
        InterfaceUtilisateurController guiUser = fxmlLoader.getController();
        guiUser.setUtilisateur(utilisateur);
        guiUser.onMenuDeroulantClick();
        Stage stage = new Stage();
        stage.setTitle("Livres disponibles");
        stage.setScene(new Scene(root1));
        stage.show();
        connectionStage.close();
    }
    @FXML
    public void onMenuDeroulantEmpruntsClick(){
        String choix = boutonMenuDeroulant.getText();
        colonneTitreEmprunt.setCellValueFactory(new PropertyValueFactory<Emprunts, String>("titre"));
        colonneAuteurEmprunt.setCellValueFactory(new PropertyValueFactory<Emprunts, String>("auteur"));
        colonneDateDebutEmprunt.setCellValueFactory(new PropertyValueFactory<Emprunts, Date>("dateDebut"));
        colonneDateFinEmprunt.setCellValueFactory(new PropertyValueFactory<Emprunts, Date>("dateFin"));
        switch (choix){
            case "Emprunts en cours" -> {
            tableEmprunts.setItems(utilisateur.getEmpruntsEnCours());
        }
            case "Emprunts finis" -> {
            tableEmprunts.setItems(utilisateur.getEmpruntsFinis());
        }
            default ->  {
            ObservableList<Emprunts> list = utilisateur.getEmpruntsEnCours();
            list.addAll(utilisateur.getEmpruntsFinis());
            tableEmprunts.setItems(list);
        }
        }
    }
    @FXML
    public void onRendreButtonClicked(){
        Emprunts emprunt  = tableEmprunts.getSelectionModel().getSelectedItem();
        if (emprunt == null){
            labelComm.setText("Vous n'avez pas séléctionné d'emprunt'");
        } else {
            try{
                Connection con = DriverManager.getConnection(
                            "jdbc:mysql://localhost:3306/DbBiblio", "root", "Sobavo+55927");
                PreparedStatement stmtVerif = con.prepareStatement("SELECT COUNT(IdEmprunt) FROM EmpruntsEnCours WHERE IdEmprunt = ?");
                PreparedStatement stmtHistorique = con.prepareStatement("INSERT INTO HistoriqueEmprunts VALUES (?,?,?,?,NOW())");
                PreparedStatement stmtEnCours = con.prepareStatement("DELETE FROM EmpruntsEnCours WHERE IdEmprunt = ?");
                stmtVerif.setInt(1, emprunt.idEmprunt);
                ResultSet rs = stmtVerif.executeQuery();
                if(rs.next()) {
                    if(rs.getInt(1)==1) {
                        stmtHistorique.setInt(1, emprunt.idEmprunt);
                        stmtHistorique.setInt(2, emprunt.idUser);
                        stmtHistorique.setInt(3, emprunt.idLivre);
                        stmtHistorique.setString(4, String.valueOf(emprunt.dateDebut));
                        stmtEnCours.setInt(1, emprunt.idEmprunt);
                        stmtHistorique.addBatch();
                        stmtHistorique.executeBatch();
                        stmtEnCours.execute();
                        labelComm.setText("Emprunt rendu");
                        onMenuDeroulantEmpruntsClick();
                    }
                    else{
                        labelComm.setText("Cet emprunt est terminé");
                    }
                }
                }
            catch (Exception e) {
                System.out.println(e);
            }
        }
    }
    public void onAccesUsersClick() throws IOException, SQLException {
        Stage connectionStage = (Stage) tableEmprunts.getScene().getWindow();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("gestion-users.fxml"));
        Parent root1 = (Parent) fxmlLoader.load();
        InterfaceAdminGestionUsersController guiAdminUsers = fxmlLoader.getController();
        guiAdminUsers.setUtilisateur(utilisateur);
        Connection con = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/DbBiblio", "root", "Sobavo+55927");
        PreparedStatement stmt = con.prepareStatement("SELECT Id, Nom, Prenom, Email, CategorieId FROM Users");
        guiAdminUsers.onRechercherButtonClick();
        PreparedStatement stmtCat = con.prepareStatement("SELECT Nom FROM Categorie");
        ResultSet rsCat = stmtCat.executeQuery();
        ObservableList<String> listeCat = FXCollections.observableArrayList();
        while(rsCat.next()){
            String nom = rsCat.getString(1);
            listeCat.add(nom);
        }
        guiAdminUsers.listeCategories.setItems(listeCat);
        guiAdminUsers.listeCategories2.setItems(listeCat);
        Stage stage = new Stage();
        stage.setTitle("Gestion des utilisateurs");
        stage.setScene(new Scene(root1));
        stage.show();
        connectionStage.close();
    }
}
