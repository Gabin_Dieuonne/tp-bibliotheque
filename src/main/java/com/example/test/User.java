package com.example.test;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;
import java.util.Objects;

public class User {
    public int id = -1;
    private SimpleStringProperty password = new SimpleStringProperty();
    public SimpleStringProperty nom = new SimpleStringProperty();
    public SimpleStringProperty prenom = new SimpleStringProperty();
    public SimpleStringProperty mail = new SimpleStringProperty();
    public Integer categorieId = 0;

    public User(int id, String nom, String prenom, String mail, Integer categorieId) {
        this.id = id;
        this.nom = new SimpleStringProperty(nom);
        this.prenom = new SimpleStringProperty(prenom);
        this.mail = new SimpleStringProperty(mail);
        this.categorieId = categorieId;
    }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom.get();
    }

    public User(int id, String password, String nom, String prenom, String mail, int categorieId) {
        this.id = id;
        this.password = new SimpleStringProperty(password);
        this.nom = new SimpleStringProperty(nom);
        this.prenom = new SimpleStringProperty(prenom);
        this.mail = new SimpleStringProperty(mail);
        this.categorieId = categorieId;
    }
    public Boolean connection(String typedPassword) {
        return (Objects.equals(typedPassword, password.get()));
    }
    public int getNombreEmpruntsDispos() {
        try {
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/DbBiblio", "root", "Sobavo+55927");
            PreparedStatement stmtCategorie = con.prepareStatement("SELECT MaxEmprunts FROM Categorie WHERE Categorie.Id = ?");
            stmtCategorie.setInt(1, categorieId);
            ResultSet rsCategorie = stmtCategorie.executeQuery();
            int empruntsDispos = 0;
            if(rsCategorie.next()){
                empruntsDispos = rsCategorie.getInt(1);
            }
            PreparedStatement stmtEmprunts = con.prepareStatement("SELECT COUNT(IdEmprunt) FROM EmpruntsEnCours WHERE IdUser = ?");
            stmtEmprunts.setInt(1, id);
            ResultSet rsEmprunts = stmtEmprunts.executeQuery();
            if(rsEmprunts.next()){
                empruntsDispos -= rsEmprunts.getInt(1);
            }
            return empruntsDispos;
        } catch (Exception e) {
            System.out.println(e);
            return 0;
        }
    }
    public ObservableList<Emprunts> getEmpruntsEnCours(){
        try{
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/DbBiblio", "root", "Sobavo+55927");
            PreparedStatement stmt = con.prepareStatement("SELECT * FROM EmpruntsEnCours WHERE EmpruntsEnCours.IdUser = ?");
            stmt.setInt(1,id);
            ResultSet rs = stmt.executeQuery();
            ObservableList<Emprunts> empruntsARentrer = FXCollections.observableArrayList();
            while (rs.next()) {
                int idEmprunt = rs.getInt(1);
                int idLivre = rs.getInt(3);
                Date dateDebut = rs.getDate(4);
                Date dateFin = rs.getDate(5);
                Livre livre = new Livre(idLivre);
                livre.livreFromId(idLivre);
                String auteurs = livre.getAuteurs();
                empruntsARentrer.add(new Emprunts(idEmprunt, idLivre, id, dateDebut, dateFin, livre.titre, auteurs));
            }
            return empruntsARentrer;
        }
        catch(Exception e){
            System.out.println(e);
            return null;
        }
    }
    public ObservableList<Emprunts> getEmpruntsFinis(){
        try{
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/DbBiblio", "root", "Sobavo+55927");
            PreparedStatement stmt = con.prepareStatement("SELECT * FROM HistoriqueEmprunts WHERE HistoriqueEmprunts.IdUser = ?");
            stmt.setInt(1,id);
            ResultSet rs = stmt.executeQuery();
            ObservableList<Emprunts> empruntsARentrer = FXCollections.observableArrayList();
            while (rs.next()) {
                int idEmprunt = rs.getInt(1);
                int idLivre = rs.getInt(3);
                Date dateDebut = rs.getDate(4);
                Date dateFin = rs.getDate(5);
                Livre livre = new Livre(idLivre);
                livre.livreFromId(idLivre);
                String auteurs = livre.getAuteurs();
                empruntsARentrer.add(new Emprunts(idEmprunt, idLivre, id, dateDebut, dateFin, livre.titre, auteurs));
            }
            return empruntsARentrer;
        }
        catch(Exception e){
            System.out.println(e);
            return null;
        }
    }
    private int getMaxId(){
        try{
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/DbBiblio", "root", "Sobavo+55927");
            PreparedStatement stmt = con.prepareStatement("SELECT MAX(id) FROM Users");
            ResultSet rs = stmt.executeQuery();
            int maxId = 1;
            if (rs.next()){
                maxId = rs.getInt(1);
            }
            return maxId;
        }
        catch(Exception e){
            System.out.println(e);
            return 1;
        }
    }
    public void setCategorieId(int categorieId){
        this.categorieId = categorieId;
        try {
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/DbBiblio", "root", "Sobavo+55927");
            PreparedStatement stmt = con.prepareStatement("UPDATE Users SET CategorieId = ? WHERE Users.Id = ?");
            stmt.setInt(1, categorieId);
            stmt.setInt(2, id);
            stmt.executeUpdate();
        }
        catch(Exception e){
            System.out.println(e);
        }
    }

    public SimpleStringProperty nomProperty() {
        return nom;
    }

    public String getPrenom() {
        return prenom.get();
    }

    public SimpleStringProperty prenomProperty() {
        return prenom;
    }

    public String getMail() {
        return mail.get();
    }

    public SimpleStringProperty mailProperty() {
        return mail;
    }

    public Integer getCategorieId() {
        return categorieId;
    }

    public User(){
        this.id = getMaxId() +1;
    }
    public int getDureeEmprunts(){
        try{
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/DbBiblio", "root", "Sobavo+55927");
            PreparedStatement stmt = con.prepareStatement("SELECT MaxDuree FROM Categorie WHERE Id = ?");
            stmt.setInt(1, categorieId);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                return rs.getInt(1);
            }
            else{
                return 0;
            }
        }
        catch(Exception e){
            System.out.println(e);
            return 0;
        }
    }
}
