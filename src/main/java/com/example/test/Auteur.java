package com.example.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.Year;

public class Auteur {
    int id;
    String nom;
    String prenom;
    String anneeNaissance;
    public Auteur(int id, String nom, String prenom, String anneeNaissance){
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.anneeNaissance = anneeNaissance;
    }
    private int getMaxId(){
        try{
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/DbBiblio", "root", "Sobavo+55927");
            PreparedStatement stmt = con.prepareStatement("SELECT MAX(Id) FROM Auteurs");
            ResultSet rs = stmt.executeQuery();
            int maxId = 1;
            if (rs.next()){
                maxId = rs.getInt(1);
            }
            return maxId;
        }
        catch(Exception e){
            System.out.println(e);
            return 1;
        }
    }
    public Auteur(){
        this.id = getMaxId()+1;
    }

    public Auteur(String nom, String prenom, String anneeNaissance) {
        this.id = getMaxId()+1;
        this.nom = nom;
        this.prenom = prenom;
        this.anneeNaissance = anneeNaissance;
    }
    public Boolean auteurExist(){
        try {
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/DbBiblio", "root", "Sobavo+55927");
            PreparedStatement stmt = con.prepareStatement("SELECT COUNT(Id) FROM Auteurs WHERE Nom = ? AND Prenom = ? AND AnneeNaissance = ?");
            stmt.setString(1, nom);
            stmt.setString(2, prenom);
            stmt.setString(3, anneeNaissance);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                return rs.getInt(1) > 0;
            }
            return false;
        }
        catch(Exception e){
            System.out.println(e);
            return false;
        }
    }
    public void getAuteurFromAllButId(String nom, String prenom, String dateNaissance){
        this.nom = nom;
        this.prenom = prenom;
        this.anneeNaissance = anneeNaissance;
        try {
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/DbBiblio", "root", "Sobavo+55927");
            PreparedStatement stmt = con.prepareStatement("SELECT Id FROM Auteurs WHERE Nom = ? AND Prenom = ? AND AnneeNaissance = ?");
            stmt.setString(1, nom);
            stmt.setString(2, prenom);
            stmt.setString(3, dateNaissance);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                this.id = rs.getInt(1);
            }
        }
        catch(Exception e){
            System.out.println(e);
        }
    }
}
