package com.example.test;

import javafx.beans.property.SimpleStringProperty;

import java.util.Vector;

public class LivreTableau {
    public final SimpleStringProperty titre;
    public final SimpleStringProperty auteur;
    public final SimpleStringProperty dateDeParution;
    public final SimpleStringProperty edition;
    public final SimpleStringProperty motsCles;
    public final SimpleStringProperty statut;
    public int idLivre;
    public LivreTableau(String titre, String auteur, String dateDeParution, String edition, String motsCles, String statut, int idLivre){
        this.titre = new SimpleStringProperty(titre);
        this.auteur = new SimpleStringProperty(auteur);
        this.dateDeParution = new SimpleStringProperty(dateDeParution);
        this.edition = new SimpleStringProperty(edition);
        this.motsCles = new SimpleStringProperty(motsCles);
        this.statut = new SimpleStringProperty(statut);
        this.idLivre = idLivre;
    }

    public String getTitre() {
        return titre.get();
    }
    public String getAuteur() {
        return auteur.get();
    }
    public int getIdLivre() {
        return idLivre;
    }

    public String getDateDeParution() {
        return dateDeParution.get();
    }

    public SimpleStringProperty dateDeParutionProperty() {
        return dateDeParution;
    }

    public String getMotsCles() {
        return motsCles.get();
    }

    public SimpleStringProperty motsClesProperty() {
        return motsCles;
    }

    public String getStatut() {
        return statut.get();
    }

    public SimpleStringProperty statutProperty() {
        return statut;
    }

    public SimpleStringProperty titreProperty() {
        return titre;
    }

    public SimpleStringProperty auteurProperty() {
        return auteur;
    }

    public String getEdition() {
        return edition.get();
    }

    public SimpleStringProperty editionProperty() {
        return edition;
    }
}
