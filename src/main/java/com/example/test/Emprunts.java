package com.example.test;

import javafx.beans.property.SimpleStringProperty;

import java.sql.*;

public class Emprunts {
    public int idEmprunt;
    public int idLivre;
    public int idUser;
    public Date dateDebut;
    public Date dateFin = null;
    public SimpleStringProperty titre;
    public SimpleStringProperty auteur;

    public Emprunts(int idEmprunt, int idLivre, int idUser, Date dateDebut, String titre, String auteur) {
        this.idEmprunt = idEmprunt;
        this.idLivre = idLivre;
        this.idUser = idUser;
        this.dateDebut = dateDebut;
        this.titre = new SimpleStringProperty(titre);
        this.auteur = new SimpleStringProperty(auteur);
    }

    private int getMaxId(){
        try{
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/DbBiblio", "root", "Sobavo+55927");
            PreparedStatement stmtEnCours = con.prepareStatement("SELECT MAX(idEmprunt) FROM EmpruntsEnCours");
            ResultSet rsEnCours = stmtEnCours.executeQuery();
            int maxId = 1;
            if (rsEnCours.next()){
                maxId = rsEnCours.getInt(1);
            }
            PreparedStatement stmtFinis = con.prepareStatement("SELECT MAX(idEmprunt) FROM HistoriqueEmprunts");
            ResultSet rsFinis = stmtFinis.executeQuery();
            if (rsFinis.next()){
                int autreMax = rsFinis.getInt(1);
                if (autreMax > maxId){
                    maxId = autreMax;
                }
            }
            return maxId;
        }
        catch(Exception e){
            System.out.println(e);
            return 1;
        }
    }

    public Emprunts(int idLivre, int idUser) {
        this.idEmprunt = getMaxId()+1;
        this.idLivre = idLivre;
        this.idUser = idUser;
    }
    public String getAuteur() {
        return auteur.get();
    }
    public Emprunts(int idLivre, int idUser, Date dateDebut){
        this.idEmprunt = getMaxId()+1;
        this.idLivre = idLivre;
        this.idUser = idUser;
        this.dateDebut = dateDebut;
    }

    public Emprunts(int idLivre, int idUser, Date dateDebut, Date dateFin) {
        this.idEmprunt = getMaxId()+1;
        this.idLivre = idLivre;
        this.idUser = idUser;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
    }
    public Emprunts(int idEmprunt, int idLivre, int idUser, Date dateDebut) {
        this.idEmprunt = idEmprunt;
        this.idLivre = idLivre;
        this.idUser = idUser;
        this.dateDebut = dateDebut;
    }

    public Emprunts(int idLivre, int idUser, Date dateDebut, Date dateFin, String titre, String auteurs) {
        this.idEmprunt = getMaxId()+1;
        this.idLivre = idLivre;
        this.idUser = idUser;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.titre = new SimpleStringProperty(titre);
        this.auteur = new SimpleStringProperty(auteurs);
    }
    public Emprunts(int idEmprunt, int idLivre, int idUser, Date dateDebut, Date dateFin, String titre, String auteurs) {
        this.idEmprunt = idEmprunt;
        this.idLivre = idLivre;
        this.idUser = idUser;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.titre = new SimpleStringProperty(titre);
        this.auteur = new SimpleStringProperty(auteurs);
    }

    public Emprunts(int idLivre, int idUser, Date dateDebut, String titre, String auteur) {
        this.idEmprunt = getMaxId()+1;
        this.idLivre = idLivre;
        this.idUser = idUser;
        this.dateDebut = dateDebut;
        this.titre = new SimpleStringProperty(titre);
        this.auteur = new SimpleStringProperty(auteur);
    }

    public int getIdEmprunt() {
        return idEmprunt;
    }

    public int getIdLivre() {
        return idLivre;
    }

    public int getIdUser() {
        return idUser;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public String getTitre() {
        return titre.get();
    }

    public SimpleStringProperty titreProperty() {
        return titre;
    }

    public SimpleStringProperty auteurProperty() {
        return auteur;
    }
}
